﻿namespace SayWhen.Domain.ValueObjects
{
	[Flags]
	public enum TimeOfDay : short
	{
		None = 0,
		Morning = 1 << 0,
		MidDay = 1 << 1,
		Evening = 1 << 2,
		AllDay = ~(~0 << 3)
	}
}
