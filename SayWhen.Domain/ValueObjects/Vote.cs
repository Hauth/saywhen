﻿namespace SayWhen.Domain.ValueObjects
{
	public class Vote
	{
		private readonly Dictionary<DateTime, TimeOfDay> datesWithTime;
		public Vote(string voterName)
		{
			this.VoterName = voterName;
			this.datesWithTime = new Dictionary<DateTime, TimeOfDay>();
		}

		public string VoterName { get; }


		public Dictionary<DateTime, TimeOfDay> DatesWithTime => datesWithTime;

		public void AddDateWithTime(KeyValuePair<DateTime, TimeOfDay> dateWithTime)
		{
			datesWithTime.Add(dateWithTime.Key, dateWithTime.Value);
		}
		public void AddDateAndTime(DateTime date, TimeOfDay time)
		{
			datesWithTime.Add(date, time);
		}
	}
}
