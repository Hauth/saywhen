﻿using System.Collections.ObjectModel;
using SayWhen.Api.Shared;
using SayWhen.Domain.ValueObjects;

namespace SayWhen.Domain
{
	public class Poll
	{
		private readonly List<DateTime> datesToChooseFrom;
		private readonly List<Vote> votes;

		public Poll(string title, string description)
		{
			this.PollId = string.Empty;
			this.Version = string.Empty;
			this.datesToChooseFrom = new List<DateTime>(10);
			this.CreatedDate = DateTimeOffset.Now;
			this.Title = title;
			this.Description = description;
			this.votes = new();
		}

		public Poll(
			string pollId,
			string version,
			string title,
			string description,
			DateTimeOffset createdDate,
			ICollection<DateTime> datesToChooseFrom)
		{
			pollId.EnsureNotEmpty();
			version.EnsureNotEmpty();
			createdDate.EnsureNotDefault();

			this.PollId = pollId;
			this.Version = version;
			this.Title = title;
			this.Description = description;
			this.CreatedDate = createdDate;
			this.datesToChooseFrom = datesToChooseFrom.ToList();
			this.votes = new();
		}

		public DateTimeOffset CreatedDate { get; }
		public ReadOnlyCollection<DateTime> DatesToChooseFrom => this.datesToChooseFrom.AsReadOnly();
		public ReadOnlyCollection<Vote> Votes => this.votes.AsReadOnly();
		public string Title { get; }
		public string Description { get; }
		public string PollId { get; private set; }
		public string Version { get; private set; }

		public void AddDatesToChooseFrom(params DateTime[] dates)
		{
			this.datesToChooseFrom.AddRange(dates.Distinct());
			this.datesToChooseFrom.Sort();
		}

		public void SetIdentity(string pollId, string version)
		{
			pollId.EnsureNotEmpty();
			version.EnsureNotEmpty();
			this.PollId = pollId;
			this.Version = version;
		}

		public Vote Vote(string voterName, ICollection<KeyValuePair<DateTime, TimeOfDay>> datesWithTime)
		{
			var vote = new Vote(voterName);
			foreach(var dateWithTime in datesWithTime.OrderBy(kvp => kvp.Key))
			{
				vote.AddDateWithTime(dateWithTime);
			}

			this.votes.Add(vote);
			return vote;
		}
	}
}
