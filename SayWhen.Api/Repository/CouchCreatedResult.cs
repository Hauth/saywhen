﻿using System.Text.Json.Serialization;

namespace SayWhen.Api.Repository
{
	public class CouchCreatedResult
	{
		[JsonPropertyName("id")]
		public string Id { get; set; } = string.Empty;

		[JsonPropertyName("ok")]
		public bool CreateSucceeded { get; set; }

		[JsonPropertyName("rev")]
		public string Version { get; set; } = string.Empty;
	}
}
