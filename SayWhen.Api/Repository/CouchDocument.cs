﻿using System.Text.Json.Serialization;

namespace SayWhen.Api.Repository
{
	public class CouchDocument
	{
		[JsonPropertyName("_id")]
		public string? Id { get; set;}

		[JsonPropertyName("_rev")]
		public string? RevisionId { get; set;}
	}
}
