﻿namespace SayWhen.Api.Repository
{
	public class PollDocument : CouchDocument
	{
		public PollDocument(
			DateTimeOffset createdDate,
			ICollection<DateTime> datesToChooseFrom,
			string title,
			string description,
			ICollection<VoteDocument> votes,
			string? id = null,
			string? revisionId = null)
		{
			this.CreatedDate = createdDate;
			this.DatesToChooseFrom = datesToChooseFrom;
			this.Title = title;
			this.Description = description;
			this.Id = id;
			this.RevisionId = revisionId;
			this.Votes = votes;
		}

		public DateTimeOffset CreatedDate { get; set; }
		public ICollection<DateTime> DatesToChooseFrom { get; set; }
		public ICollection<VoteDocument> Votes { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
	}
}
