﻿using SayWhen.Domain.ValueObjects;

namespace SayWhen.Api.Repository
{
	public class VoteDocument
	{
		public string VoterName { get; set; }

		public IDictionary<DateTime, TimeOfDay> DatesWithTime { get; set; }
	}
}
