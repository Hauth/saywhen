﻿using SayWhen.Api.Shared.Contracts.Commands;
using SayWhen.Domain;
using System.Text.Json;

namespace SayWhen.Api.Repository
{
	public class PollRepository
	{
		private readonly HttpClient httpClient;
		private readonly ILogger<PollRepository> logger;
		private static readonly JsonSerializerOptions IgnoreNullProperties = new()
		{
			DefaultIgnoreCondition = System.Text.Json.Serialization.JsonIgnoreCondition.WhenWritingDefault
		};

		public PollRepository(HttpClient client, ILogger<PollRepository> logger)
		{
			this.httpClient = client;
			this.logger = logger;
		}

		public async Task<Poll?> ReadPoll(string pollId)
		{
			var response = await this.httpClient.GetAsync(pollId);
			if (response.IsSuccessStatusCode)
			{
				var document = await response.Content.ReadFromJsonAsync<PollDocument>();
				if (document == null) return null;

				var poll = new Poll(
				pollId: document.Id!,
				version: document.RevisionId!,
				title: document.Title,
				description: document.Description,
				createdDate: document.CreatedDate,
				datesToChooseFrom: document.DatesToChooseFrom);
				foreach (var vote in document.Votes)
				{
					poll.Vote(vote.VoterName, vote.DatesWithTime);
				}
				return poll;
			}

			return null;
		}

		public async Task<Poll?> CreatePoll(CreatePollCommand command)
		{
			var poll = new Poll(command.Title, command.Description);
			poll.AddDatesToChooseFrom(command.DatesToChooseFrom.ToArray());

			var document = new PollDocument(
				createdDate: DateTimeOffset.Now,
				datesToChooseFrom: command.DatesToChooseFrom,
				title: command.Title,
				description: command.Description,
				votes: Array.Empty<VoteDocument>());

			var response = await this.httpClient.PostAsJsonAsync("", document, IgnoreNullProperties);
			var result = await response.Content.ReadFromJsonAsync<CouchCreatedResult>();
			if (result!.CreateSucceeded)
			{
				poll.SetIdentity(result.Id, result.Version);
				return poll;
			}
			else
			{
				this.logger.LogError("Unable to create Poll because: {reason}.", await response.Content.ReadAsStringAsync());
			}

			return null;
		}

		public async Task UpdatePoll(Poll poll)
		{
			var document = new PollDocument(
					id: poll.PollId,
					revisionId: poll.Version,
					createdDate: poll.CreatedDate,
					datesToChooseFrom: poll.DatesToChooseFrom,
					title: poll.Title,
					description: poll.Description,
					votes: poll.Votes.Select(v => new VoteDocument { VoterName = v.VoterName, DatesWithTime = v.DatesWithTime }).ToList()
				);

			var response = await this.httpClient.PutAsJsonAsync(poll.PollId, document);
			var result = await response.Content.ReadFromJsonAsync<CouchCreatedResult>();
			if (!result!.CreateSucceeded)
			{
				throw new InvalidOperationException("Tried updating a poll without having the latest version.");
			}
		}
	}
}
