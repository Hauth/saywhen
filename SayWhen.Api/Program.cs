﻿using System.Text;
using SayWhen.Api.Repository;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;
// Add services to the container.

builder.Services
	.AddControllers()
	.AddJsonOptions(options =>
	{
		options.JsonSerializerOptions.WriteIndented = true;
		options.JsonSerializerOptions.PropertyNamingPolicy = null;
	});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddHttpClient<PollRepository>(client =>
{
	var base64 = Convert.ToBase64String(Encoding.UTF8.GetBytes("admin:admin"));
	client.DefaultRequestHeaders.Add("Accept", "application/json");
	client.DefaultRequestHeaders.Add("Authorization", $"Basic {base64}");

	var baseUrl = $"{configuration["CouchDB:Url"]}/{configuration["CouchDB:Database"]}/";
	client.BaseAddress = new Uri(baseUrl, UriKind.Absolute);
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
	app.UseHttpsRedirection(); // Only in dev because Release versions are behind an apache server which handles https.
}

app.UseSwagger();
app.UseSwaggerUI();

app.UseCors(policy =>
{
	policy
		.AllowAnyMethod()
		.AllowAnyHeader()
		.AllowAnyOrigin();
});

app.UseAuthorization();

app.MapControllers();
app.Map("/", () => "Ok");

app.Run();
