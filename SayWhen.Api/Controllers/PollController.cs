﻿using Microsoft.AspNetCore.Mvc;
using SayWhen.Api.Repository;
using SayWhen.Api.Shared.Contracts.Commands;
using SayWhen.Api.Shared.Contracts.Types;

namespace SayWhen.Api.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class PollsController : ControllerBase
	{
		private readonly PollRepository pollRepository;

		public PollsController(PollRepository pollRepository)
		{
			this.pollRepository = pollRepository;
		}

		[HttpGet("{pollId}")]
		public async Task<IActionResult> GetPoll(string pollId)
		{
			var poll = await this.pollRepository.ReadPoll(pollId);
			if (poll == null)
			{
				return this.NotFound();
			}

			var okResult = new Poll()
			{
				Id = poll.PollId,
				ChosenDates = poll.DatesToChooseFrom,
				CreatedDate = poll.CreatedDate,
				Description = poll.Description,
				Title = poll.Title,
				Votes = poll.Votes.Select(v => new Vote
				{
					DatesWithTime = v.DatesWithTime.ToDictionary(kvp => kvp.Key, kvp => (TimeOfDay)kvp.Value),
					VoterName = v.VoterName
				}).ToList()
			};

			return this.Ok(okResult);
		}

		[HttpPost]
		public async Task<IActionResult> CreatePoll([FromBody] CreatePollCommand command)
		{
			var poll = await this.pollRepository.CreatePoll(command);
			if (poll == null)
			{
				return this.NoContent();
			}

			var createdResult = new Poll()
			{
				Id = poll.PollId,
				ChosenDates = poll.DatesToChooseFrom,
				CreatedDate = poll.CreatedDate,
				Description = poll.Description,
				Title = poll.Title
			};
			return this.CreatedAtAction(
				nameof(this.GetPoll),
				new { pollId = poll.PollId },
				createdResult);
		}

		[HttpPost("{pollId}/Vote")]
		public async Task<IActionResult> VoteOnPoll([FromRoute] string pollId, [FromBody] VoteOnPollCommand command)
		{
			var poll = await this.pollRepository.ReadPoll(pollId);
			if (poll == null)
			{
				return this.NotFound();
			}

			var datesWithTime = command.ChosenDatesWithTime.ToDictionary(kvp => kvp.Key, kvp => (Domain.ValueObjects.TimeOfDay)kvp.Value);
			poll.Vote(command.VoterName, datesWithTime);

			await this.pollRepository.UpdatePoll(poll);

			return this.Ok();
		}
	}
}
