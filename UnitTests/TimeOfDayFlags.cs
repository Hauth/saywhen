﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SayWhen.Domain.ValueObjects;

namespace UnitTests
{
	[TestClass]
	public class TimeOfDayFlags
	{
		[TestMethod]
		public void AllDayMatches()
		{
			TimeOfDay.AllDay.Should().Be(TimeOfDay.Evening | TimeOfDay.MidDay | TimeOfDay.Morning);
		}

		[TestMethod]
		public void NoneMatches()
		{
			TimeOfDay.None.Should().Be(TimeOfDay.AllDay & ~TimeOfDay.Morning & ~TimeOfDay.MidDay & ~TimeOfDay.Evening);
		}
	}
}
