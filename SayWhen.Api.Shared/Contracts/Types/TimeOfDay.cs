﻿namespace SayWhen.Api.Shared.Contracts.Types
{
	[Flags]
	public enum TimeOfDay : short
	{
		None = 0,
		Morning = 1 << 0,
		MidDay = 1 << 1,
		Evening = 1 << 2,
		AllDay = ~(~0 << 3)
	}

	public static class TimeOfDayExtensions
	{
		public static bool IsMorning(this TimeOfDay self) => (self & TimeOfDay.Morning) == TimeOfDay.Morning;
		public static bool IsMidDay(this TimeOfDay self) => (self & TimeOfDay.MidDay) == TimeOfDay.MidDay;
		public static bool IsEvening(this TimeOfDay self) => (self & TimeOfDay.Evening) == TimeOfDay.Evening;
		public static bool IsAllDay(this TimeOfDay self) => (self & TimeOfDay.AllDay) == TimeOfDay.AllDay;
	}
}
