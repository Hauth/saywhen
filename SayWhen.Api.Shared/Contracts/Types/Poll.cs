﻿namespace SayWhen.Api.Shared.Contracts.Types
{
	public class Poll
	{
		public string Id { get; set; } = string.Empty;
		public string Title { get; set; } = string.Empty;
		public string Description { get; set; } = string.Empty;
		public ICollection<DateTime> ChosenDates { get; set; } = new List<DateTime>(0);
		public DateTimeOffset CreatedDate { get; set; }
		public ICollection<Vote> Votes { get; set; } = new List<Vote>(0);
	}
}
