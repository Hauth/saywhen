﻿namespace SayWhen.Api.Shared.Contracts.Types
{
	public class Vote
	{
		public string VoterName { get; set; }
		public IDictionary<DateTime, TimeOfDay> DatesWithTime { get; set; }
	}
}
