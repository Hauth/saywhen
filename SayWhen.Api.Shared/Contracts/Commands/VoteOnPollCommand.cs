﻿using System.ComponentModel.DataAnnotations;
using SayWhen.Api.Shared.Contracts.Types;

namespace SayWhen.Api.Shared.Contracts.Commands
{
	public class VoteOnPollCommand
	{
		public VoteOnPollCommand()
		{
			VoterName = string.Empty;
			ChosenDatesWithTime = new List<KeyValuePair<DateTime, TimeOfDay>>();
		}

		[Required]
		public string VoterName { get; set; }

		[MinLength(1)]
		public ICollection<KeyValuePair<DateTime, TimeOfDay>> ChosenDatesWithTime { get; set; }
	}
}
