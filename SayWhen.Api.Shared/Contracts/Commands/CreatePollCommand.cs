﻿using System.ComponentModel.DataAnnotations;

namespace SayWhen.Api.Shared.Contracts.Commands
{
	public class CreatePollCommand
	{
		public CreatePollCommand()
		{
			this.DatesToChooseFrom = Array.Empty<DateTime>();
			this.Title = string.Empty;
			this.Description = string.Empty;
		}

		[MinLength(1)]
		public ICollection<DateTime> DatesToChooseFrom { get; set; }

		[Required]
		public string Title { get; set; }
		public string Description { get; set; }
	}
}
