﻿using System.Runtime.CompilerServices;

namespace SayWhen.Api.Shared
{
	public static class ValidationExtensions
	{
		public static void EnsureNotEmpty(this string s, [CallerArgumentExpression("s")] string paramName = "")
		{
			if (string.IsNullOrWhiteSpace(s))
			{
				throw new ArgumentException(null, paramName);
			}
		}

		public static void EnsureNotDefault(this DateTimeOffset dto, [CallerArgumentExpression("dto")] string paramName = "")
		{
			if (dto == default)
			{
				throw new ArgumentException(null, paramName);
			}
		}
	}
}
