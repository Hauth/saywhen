﻿using System.ComponentModel.DataAnnotations;

namespace SayWhen.UI.Components
{
	public class NewPollFormAnswers
	{
		[Required]
		[StringLength(maximumLength: 50, MinimumLength = 1, ErrorMessage = "Title size limit is 50 characters.")]
		public string Title { get; set; } = string.Empty;

		[MaxLength(500, ErrorMessage = "Description size limit is 500 characters.")]
		public string Description { get; set; } = string.Empty;
	}
}
