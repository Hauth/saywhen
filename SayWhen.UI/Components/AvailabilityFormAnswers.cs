﻿using System.ComponentModel.DataAnnotations;
using SayWhen.Api.Shared.Contracts.Types;

namespace SayWhen.UI.Components
{
	public class AvailabilityFormAnswers
	{
		[Required(ErrorMessage = "A name is required.")]
		[StringLength(maximumLength: 50, MinimumLength = 1, ErrorMessage = "Name size limit is 50 characters.")]
		public string Name { get; set; } = string.Empty;

		[MinLength(1, ErrorMessage = "Select at least one date.")]
		public Dictionary<DateTime, TimeOfDay> DatesAndTimes = new();
	}
}
