﻿using System.Net.Http.Json;
using SayWhen.Api.Shared.Contracts.Commands;
using SayWhen.Api.Shared.Contracts.Types;
using SayWhen.UI.Components;

namespace SayWhen.UI
{
	public class ApiClient
	{
		public ApiClient(HttpClient client)
		{
			this.Client = client;
		}

		public HttpClient Client { get; }

		public async Task<Poll?> CreatePollAsync(NewPollFormAnswers answers, IList<DateTime> chosenDates)
		{
			var body = new CreatePollCommand()
			{
				DatesToChooseFrom = chosenDates,
				Title = answers.Title,
				Description = answers.Description
			};
			var response = await this.Client.PostAsJsonAsync(new Uri("Polls", UriKind.Relative), body);
			if (response.IsSuccessStatusCode)
			{
				return await response.Content.ReadFromJsonAsync<Poll>();
			}

			return null;
		}

		public async Task<Poll?> GetPollAsync(string pollId)
		{
			var response = await this.Client.GetAsync(new Uri($"Polls/{pollId}", UriKind.Relative));
			if (response.IsSuccessStatusCode)
			{
				return await response.Content.ReadFromJsonAsync<Poll>();
			}

			return null;
		}

		public async Task<bool> VoteOnPollAsync(string pollId, string voterName, IDictionary<DateTime, TimeOfDay> datesAndTimes)
		{
			var command = new VoteOnPollCommand()
			{
				ChosenDatesWithTime = datesAndTimes,
				VoterName = voterName
			};

			var response = await this.Client.PostAsJsonAsync($"Polls/{pollId}/Vote", command);
			return response.IsSuccessStatusCode;
		}
	}
}
