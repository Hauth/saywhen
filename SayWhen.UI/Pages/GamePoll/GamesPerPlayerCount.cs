﻿namespace SayWhen.UI.Pages.GamePoll
{
	public static class GamesPerPlayerCount
	{
		/// <summary>
		/// Key is game title. Value is player count.
		/// </summary>
		public static readonly IReadOnlyDictionary<string, PlayerCountRange> Lookup = new Dictionary<string, PlayerCountRange>()
		{
			{ "Back 4 Blood", new PlayerCountRange(2, 4) },
			{ "Deep Rock Galactic", new PlayerCountRange(4) },
			{ "Heroes of the Storm", new PlayerCountRange(3,5) },
			{ "R6 Siege", new PlayerCountRange(4,5) },
			{ "Aliens", new PlayerCountRange(3) },
			{ "Evolve: Stage 2", new PlayerCountRange(5) },
			{ "Paladins", new PlayerCountRange(3,5) },
			{ "Pulsar", new PlayerCountRange(3,5) },
			{ "Satisfactory", new PlayerCountRange(2,4) },
			{ "Risk of Rain", new PlayerCountRange(2,4) },
			{ "Valheim", PlayerCountRange.AnyNumberOfPlayers },
			{ "Red Solstice 2", new PlayerCountRange(2, 4) },
			{ "Overwatch", new PlayerCountRange(3, 6) },
			{ "Ready or Not", new PlayerCountRange(3, 5) },
			{ "Battlefield 2042", PlayerCountRange.AnyNumberOfPlayers }
		};
	}
}
