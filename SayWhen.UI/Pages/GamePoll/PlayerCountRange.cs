﻿namespace SayWhen.UI.Pages.GamePoll
{
	public class PlayerCountRange
	{
		public static readonly PlayerCountRange AnyNumberOfPlayers = new(0);

		public PlayerCountRange(int count) : this(count, count)
		{
		}

		public PlayerCountRange(int min, int max)
		{
			MinimumPlayerCount = min;
			MaximumPlayerCount = max;
		}

		public int MinimumPlayerCount { get; }
		public int MaximumPlayerCount { get; }

		public bool IsInRange(int playerCount)
		{
			return IsAnyNumberOfPlayers || (playerCount <= MaximumPlayerCount && playerCount >= MinimumPlayerCount);
		}

		public bool IsAnyNumberOfPlayers => MinimumPlayerCount == 0 && MaximumPlayerCount == 0;
	}
}
